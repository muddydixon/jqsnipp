var app = module.parent.exports
, twitter = app.conf.auth
, locale = app.locale
, merge = app.merge
, cols = app.cols
, qs = require('querystring')
, appBase = app.conf.appBase
, OAuth = require('oauth').OAuth
, oa = new OAuth(twitter.reqTokenUrl, twitter.accTokenUrl,
								 twitter.consumerKey, twitter.consumerSecret,
								 '2.0', null, 'HMAC-SHA1');

app.all('/auth', function(req, res, next){
		next();
});

app.get('/auth', function(req, res){
		oa.getOAuthAccessToken(req.query.oauth_token, req.query.oauth_verifie, function(err, accessToken, accessTokenSecret, result){
				req.session.twitter = {
						accessToken: accessToken,
						id: result.user_id,
						name: result.screen_name
				};
				res.redirect('/')
		});
});

app.get('/auth/login', function(req, res){
		oa.getOAuthRequestToken(function(err, oauth_token, oauth_token_secret, results){
				if(err)
						return res.redirect('/');
				res.redirect(twitter.authUrl + '?' + qs.stringify({oauth_token: oauth_token, callback_url: 'http://172.19.173.112:3000/auth'}));
		});
});

app.get('/auth/logout', function(req, res){
		req.session.twitter = false;
		res.redirect('/');
});
