var app = module.parent.exports
, locale = app.locale
, merge = app.merge
, cols = app.cols
, useragent = require('useragent');

var requireSessionAPI = function(req, res, next){
		if(req.session.twitter){
				next();
		}else{
				res.send({err: {code: 401, message: 'not authorlized'}, result: null});
		}
}
var requireSession = function(req, res, next){
		if(req.session.twitter){
				next();
		}else{
				res.redirect('/');
		}
}


// /snippet/:id get/post
// /snippet/fork/:id get/post
// /snippet/new post

app.get('/snippet', function(req, res){
		cols.Snippet.find({delete: {'$exists': false}}, {}, {}, function(err, snippets){
				return res.partial('snippet', {
						session: req.session.twitter,
						collection: snippets
				});
		});
});

app.get('/snippet/new', requireSession, function(req, res){
		res.render('snippet/new', {
				session: req.session.twitter,
				title: locale.jp.title,
				description: locale.jp.description,
				snippet: null,
				libs: ['jquery-1.6.2.min.js']
		});
})

app.post('/snippet/new', requireSession, function(req, res){
		req.body.tag = req.body.tag || '*';
		if((req.body.tag || req.body.cls) &&
			 req.body.js && req.body.html){
				var snippet = new cols.Snippet({
						desc: req.body.description,
						tag: req.body.tag ? req.body.tag : '',
						cls: req.body.cls ? req.body.cls : '',
						tags: [],
						deps: [],
						css: req.body.css,
						js: req.body.js,
						html: req.body.html,
						author: {
								id: req.session.twitter.id,
								name: req.session.twitter.name
						},
						like: 0,
 						supports: [],
						created: new Date().getTime()
				});
				snippet.save(function(err){
						if(err)
								return res.redirect('/snippet/new');
						res.redirect('/');
				});
		}else{
				res.redirect('/snippet/new');
		}
})


app.get('/snippet/:id', function(req, res){
		cols.Snippet.findOne({_id: req.params.id}, function(err, snippet){
				return res.render('snippet', {
						session: req.session.twitter,
						title: locale.jp.title,
						description: locale.jp.description,
						snippet: snippet
				});
		});
});

app.post('/snippet/:id', requireSession, function(req, res){
		cols.Snippet.findOne({_id: req.params.id}, function(err, snippet){
				snippet.desc = req.body.description;
				snippet.tag = req.body.tag;
				snippet.cls = req.body.cls;
				snippet.tags = req.body.tags.match(/^\s*$/) ? [] : req.body.tags.split(/\s*,\s*/),
				snippet.css = req.body.css;
				snippet.js = req.body.js;
				snippet.html = req.body.html;
				snippet.save(function(){
						res.redirect('/');
				})
		});
});

app.get('/snippet/detail/:id', function(req, res){
		cols.Snippet.findOne({_id: req.params.id}, function(err, snippet){
				return res.partial('snippet/detail', {
						snippet:snippet,
						session: req.session.twitter
				});
		});
});

app.get('/snippet/edit/:id', requireSession, function(req, res){
		cols.Snippet.findOne({_id: req.params.id}, function(err, snippet){
				if(err)
						throw new Exception("not found error");
				res.render('snippet/new', {
						session: req.session.twitter,
						title: locale.jp.title,
						description: locale.jp.description,
						snippet: snippet,
						libs: ['jquery-1.6.2.min.js'],
						fork: false
				});
		});
});

app.get('/snippet/fork/:id', requireSession, function(req, res){
		cols.Snippet.findOne({_id: req.params.id}, function(err, snippet){
				if(err)
						throw new Exception("not found error");
				snippet.author = {id: req.session.twitter.id, name: req.session.twitter.name};
				res.render('snippet/new', {
						session: req.session.twitter,
						title: locale.jp.title,
						description: locale.jp.description,
						snippet: snippet,
						libs: ['jquery-1.6.2.min.js'],
						fork: true
				});
		});
});

app.post('/snippet/fork/:id', requireSession, function(req, res){
		req.body.tag = req.body.tag || '*';
		if((req.body.tag || req.body.cls) &&
			 req.body.js && req.body.html){
				var snippet = new cols.Snippet({
						desc: req.body.description,
						tag: req.body.tag ? req.body.tag : '',
						cls: req.body.cls ? req.body.cls : '',
						tags: [],
						deps: [],
						css: req.body.css,
						js: req.body.js,
						html: req.body.html,
						author: {
								id: req.session.twitter.id,
								name: req.session.twitter.name
						},
						like: 0,
 						supports: [],
						created: new Date().getTime()
				});
				snippet.save(function(err){
						if(err)
								return res.redirect('/snippet/new');
						res.redirect('/');
				});
		}else{
				res.redirect('/snippet/new');
		}
});

app.get('/snippet/delete/:id', requireSession, function(req, res){
		cols.Snippet.findOne({_id: req.params.id}, function(err, snippet){
				if(err)
						throw new Exception("db error");
				if(!snippet)
						throw new Exception("not found "+req.params.id);
				if(snippet.author.id !== req.session.twitter.id)
						throw new Exception("this snippet is not yours");
				console.log(snippet);
				snippet.delete = new Date().getTime();
				snippet.save(function(err){
						if(err)
								throw new Exception("db error");
						console.log(snippet);
						res.redirect('/');
				});
		});
});
app.delete('/snippet/delete/:id', requireSession, function(req, res){
		cols.Snippet.update({_id: req.params.id}, {'$set': {delete: true}}, function(err){
				res.redirect('/');
		});
});
