var app = module.exports = module.parent.exports
, fs = require('fs')
, path = require('path')
, files = fs.readdirSync(__dirname)
, locale = app.locale
, cols = app.cols
, merge = app.merge = require('connect').utils.merge
, crypto = require('crypto')
, md5 = app.md5 = function(str){
    var hash = crypto.createHash('md5');
    hash.update(str);
    return hash.digest('hex');
}

for(var i in files){
  if(files[i] !== path.basename(__filename) && files[i].indexOf('.') !== 0 && files[i].indexOf('~') !== (files[i].length - 1)){
			try{
					require(__dirname + '/' + files[i]);
			}catch(err){
					console.log('err = '+err);
			}
  }
}

app.get('/', function(req, res){
		res.render('index', {
				err: res.local('err'),
				session: req.session.twitter,
				title: locale.jp.title,
				description: locale.jp.description,
				snippets: []
		});
});
