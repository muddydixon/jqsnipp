var app = module.parent.exports
, locale = app.locale
, merge = app.merge
, cols = app.cols
, fs = require('fs');

var saveJsFile = function(file, snippets, cb){
		var filepath = __dirname + '/../public/files/'+file.author.name+'-'+file.name+'.'+file.created+'.js';
		var code = ['$(function(){']
		snippets.forEach(function(snippet){
				if(snippet.js){
						var spec = (snippet.tag ? snippet.tag : '') + (snippet.author && snippet.author.name && snippet.cls ? '.' + snippet.author.name + '-' + snippet.cls : '');
						code.push("$('"+spec+"').each(function(__CNT__){");
						code.push(snippet.js.replace(/CLASS/g, function(){return snippet.author.name + '-' + snippet.cls}));
						code.push("})");
				}
		});
		code.push('});');
		fs.writeFile(filepath, code.join('\n'), 'utf8', function(err){
				if(err)
						return cb(err, null);
				return cb(null, filepath);
		});
};
app.get('/file/download/:id', function(req, res){
		cols.File.findOne({_id: req.params.id}, function(err, file){
				if(err) throw err;
				cols.Snippet.find({_id: {'$in': file.sids}}, function(err, snippets){
						if(err) throw err;
						saveJsFile(file, snippets, function(err, filepath){
								if(err){
										return false;
								}
								res.redirect('/files/'+file.author.name+'-'+file.name+'.'+file.created+'.js');
						})
				});
		});
});
app.get('/file/:id', function(req, res){
		cols.File.findOne({_id: req.params.id}, function(err, file){
				if(err) throw err;
				cols.Snippet.find({_id: {'$in': file.sids}}, function(err, snippets){
						if(err) throw err;
						res.render('file', {
								session: req.session.twitter,
								title: locale.jp.title,
								description: locale.jp.description,
								file: file,
								snippets: snippets
						});
				})
		})
});

app.post('/file/:id', function(req, res){
		console.log(req.params.id);
		console.log(req.body);
});

app.delete('/file/:id', function(req, res){
		console.log(req.params.id);
		console.log(req.body);
});
