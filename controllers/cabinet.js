var app = module.parent.exports
, locale = app.locale
, merge = app.merge
, cols = app.cols;

// cabinetの中身を見る
app.get('/cabinet', function(req, res){
		if(req.session && req.session.twitter){
				cols.File.find({'author.id': req.session.twitter.id}, function(err, files){
						if(err) throw err;
						res.send(files);
				});
		}else{
				res.send([]);
		}
});

// cabinetにfileを追加する
app.post('/cabinet', function(req, res){
		if(!req.session.twitter){
				res.local('err', {msg: 'Not Authorized'});
				return res.redirect('/');
		}
		
		if(!req.body.filename)
				return res.send({err: 'no filename', result: null});
		if(req.body.sids.length === 0)
				return res.send({err: 'there is no snippet', result: null});
				
		var file = new cols.File({
				sids: req.body.sids,
				author: {
						id: req.session.twitter.id,
						name: req.session.twitter.name
				},
				name: req.body.filename,
				created: new Date().getTime()
		});
		file.save(function(err){
				if(err){
						return res.send(err);
				}
				res.send({err: null, result: {msg: 'OK'}});
		})
});

// cartからfileを削除する
app.delete('/cabinet/:id', function(req, res){
});

