var app = module.parent.exports
, locale = app.locale
, merge = app.merge
, cols = app.cols
, useragent = require('useragent');

app.get('/support/:id', function(req, res){
		var query = {sid: req.params.id};
		if(req.query.dt)
				query.created = {'$gt': req.query.dt};
		cols.Support.find(query, function(err, supports){
				var _supports = {};
				supports.forEach(function(support){
						if(!_supports.hasOwnProperty(support.browser)){
								_supports[support.browser] = {sum: 0, vers: {}};
						}
						if(!_supports[support.browser].vers.hasOwnProperty(support.ver)){
								_supports[support.browser].vers[support.ver] = 0;
						}
						_supports[support.browser].sum++;
						_supports[support.browser].vers[support.ver]++;
				});
				return res.send(_supports);
		});
});

app.post('/support/:id', function(req, res){
 		var uaObj = useragent.parse(req.headers['user-agent']);

		var support = new app.cols.Support({
				sid: req.params.id,
				browser: uaObj.family,
				ver: uaObj.V1,
				created: new Date().getTime()
		});
		if(req.session.twitter){
				support.user = {
						id: req.session.twitter.id,
						name: req.session.twitter.name
				};
		}
		support.save(function(err){
				if(err)
						res.send({err:err});
				res.send({browser: uaObj.family, ver: uaObj.V1});
		});
});
