var app = module.parent.exports
, locale = app.locale
, merge = app.merge
, cols = app.cols
, useragent = require('useragent');

app.get('/like/:id', function(req, res){
		cols.Like.count({sid: req.params.id}, function(err, cnt){
				if(err){
						return res.send({err: {code: 500, message: err}, result: null});
				}
				res.send({err: null, result: cnt});
		});
});
app.post('/like/:id', function(req, res){
		if(!req.session.twitter){
				return res.send({err: {code: 401, message: 'please login'}, result: null});
		}
		var like = new cols.Like({
				sid: req.params.id,
				user: {
						name: req.session.twitter.name,
						id: req.session.twitter.id,
				},
				created: new Date().getTime()
		});
		like.save(function(err){
				if(err){
						return res.send({err: {code: 500, message: err}, result: null});
				}
				res.send({err: null, result: true});
		});
});
