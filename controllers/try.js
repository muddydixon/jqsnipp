var app = module.parent.exports
, locale = app.locale
, merge = app.merge
, cols = app.cols
, appBase = app.conf.appBase;

app.all('/try', function(req, res, next){
		next();
});
app.get('/try/:id', function(req, res){
		cols.Snippet.findOne({_id: req.params.id}, function(err, snippet){
				if(err){
						return res.render('tryError', {
								layout: false,
								title: 'Sorry, Server Error',
								description: locale.jp.description,
								snippet: {html: '<h2>Sorry, Server Error</h2><p>'+err+'</p>'},
								custom: ''
						});
				}
				if(!snippet){
						return res.render('tryError', {
								layout: false,
								title: 'Sorry, Not Found',
								description: locale.jp.description,
								snippet: {html: '<h2>Sorry, Not Found</h2><p>'+err+'</p>'},
								custom: ''
						});
				}
				res.render('try', {
						layout: false,
						title: locale.jp.title,
						description: locale.jp.description,
						snippet: snippet,
						custom: ''
				});
		});
});

app.get('/try/file/:snippetIds', function(req, res){
		var snippetIds = req.params.snippetIds.split(/\+/).filter(function(o){ if(o) return true});
		cols.Snippet.find({_id: {'$in': snippetIds}}, function(err, snippets){
				if(err){
						return res.render('tryFileError', {
								layout: false,
								title: 'Sorry, Server Error',
								description: locale.jp.description,
								snippets: [{html: '<h2>Sorry, Server Error</h2><p>'+err+'</p>'}],
								custom: ''
						});
				}
				if(!snippets){
						return res.render('tryFileError', {
								layout: false,
								title: 'Sorry, Server Error',
								description: locale.jp.description,
								snippets: [{html: '<h2>Sorry, Server Error</h2><p>'+err+'</p>'}],
								custom: ''
						});
				}
				var jss = '', csss = '', htmls = '';
				snippets.forEach(function(snippet){
						var className = snippet.author.name + '-' + snippet.cls;
						var spec = snippet.tag + '.' + className;
						
						jss += snippet.js ?
								"$('div#tryBody "+spec+"').each(function(){"+snippet.js.replace(/CLASS/g, function(){return className})+";});\n": ' ';
						csss += snippet.css ? snippet.css.replace(/CLASS/g, function(){return className}) : ' ';
						htmls += snippet.html ? snippet.html.replace(/CLASS/g, function(){return className}) : ' ';
				});
				res.render('tryFile', {
						layout: false,
						title: locale.jp.title,
						description: locale.jp.description,
						snippets: snippets || [],
						jss: jss,
						csss: csss,
						htmls: htmls,
						custom: ''
				});
		});
})

