var app = module.exports = module.parent.exports
, conf = app.conf
, cols = app.cols = {}
, mongoose = app.mongoose = require('mongoose')
, fs = require('fs')
, path = require('path')
, files = fs.readdirSync(__dirname)
, uri = 'mongodb://' + (conf.database.auth ? conf.database.auth + '@' : '') + conf.database.host+(conf.database.port ? ':'+conf.database.port : '') + '/' + conf.database.db ;

mongoose.connect(uri);
console.log('connect '+uri);
for(var i in files){
  if(files[i] !== path.basename(__filename) && files[i].indexOf('.') !== 0 && files[i].indexOf('~') !== (files[i].length - 1)){
    require(__dirname + '/' + files[i]);
  }
}
