var app = module.parent.exports;
var mongoose = app.mongoose;

var File = new mongoose.Schema({
		sids: [mongoose.Schema.ObjectId],
		author: {},
		name: String,
		desc: String,
		like: Number,
		created: Number
});
File.index({'author.id': 1, name: 1}, {unique: true});
mongoose.model('File', File);

app.cols.File = mongoose.model('File');

