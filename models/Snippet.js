var app = module.parent.exports;
var mongoose = app.mongoose;

var Snippet = new mongoose.Schema({
		desc: String,
		tag: String,
		cls: String,
		js: String,
		html: String,
		css: String,
		readme: String,
		deps: [String],
		fork: [mongoose.Schema.ObjectId],
		parent: mongoose.Schema.ObjectId,
		tags: [String],
		author: {},
		created: Number,
		updated: Number,
		like: Number,
		delete: Number
});

Snippet.statics.findFull = function(query, option, cb){
		if(typeof option === 'function'){
				cb = option;
				option = null;
		}
		var opt = {};
		if(option){
				['limit'].forEach(function(attr){
						if(option.hasOwnProperty(attr)){
								opt[attr] = option[attr];
						}
				});
		}
		app.cols.Snippet.find(query, {}, opt, function(err, snippets){
				if(err){
						return cb(err, null);
				}
				var clone = function clone (org, deep){
						var ret = org instanceof Array ? [] :
								(typeof org === 'object' ? {} : false);
						if(!ret){
								return org;
						}
						if(org instanceof Array){
								org.forEach(function(o){
										ret.push(clone(o));
								})
								return ret;
						}else{
								['_id', 'desc', 'tag', 'cls', 'readme', 'fork', 'parent', 'tags', 'created', 'updated', 'like', 'author'].forEach(function(attr){
										ret[attr] = org[attr];
								})
								return ret;
						}
				};
				var _snippets = clone(snippets);
				var isComp = snippets.length * 2;
				var ret = false;
				_snippets.forEach(function(snippet, i){
						isComp[i] = 0;
						app.cols.Like.find({sid: snippet._id}, function(err, likes){
								if(!ret){
										if(err){
												return cb(err, null);
										}
										snippet.liked = likes;
										if(--isComp === 0){
												return cb(null, _snippets);
										}
								}
						});
						app.cols.Support.findOne({sid: snippet._id}, function(err, supports){
								if(!ret){
										if(err){
												return cb(err, null);
										}
										snippet.supported = supports;
										if(--isComp === 0){
												return cb(null, _snippets);
										}
								}
						});
				})
		});
};

mongoose.model('Snippet', Snippet);
app.cols.Snippet = mongoose.model('Snippet');


