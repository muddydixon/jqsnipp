var app = module.parent.exports;
var mongoose = app.mongoose;

var Like = new mongoose.Schema({
		sid: mongoose.Schema.ObjectId,
		user: {},
		created: Number
});
Like.index({sid: 1, 'user.id': 1}, {unique: true});
mongoose.model('Like', Like);

app.cols.Like = mongoose.model('Like');

