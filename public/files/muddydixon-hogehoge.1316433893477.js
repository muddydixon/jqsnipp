$(function(){
$('a.Karamatsu2-blue').each(function(i){
$(this).css({color:'blue', 'font-weight': 'bold'})
})
$('div.muddydixon-areaExpand').each(function(i){
var head = $(this).find('.muddydixon-areaExpand-head');
var body = $(this).find('.muddydixon-areaExpand-body');
body.hide();
$(this).click(function(){
  body.toggle();
  if(head.text() === 'Open'){
    head.text('Close');
  }else{
    head.text('Open');
  }
})

})
$('a.muddydixon-blink').each(function(i){
var self = this;
var interval = $(this).data('blinkInterval') || 1000;
setInterval(function(){
  $(self).animate({opacity: 0},
    interval / 2, null,
    function(){
      $(self).animate({opacity: 1},
      interval / 2);
  }
)}, interval)
})
});