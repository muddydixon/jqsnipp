$(function(){
$('div.nifty_engineer-expand').each(function(__CNT__){
var head = $(this).find('.nifty_engineer-expand-head');
var body = $(this).find('.nifty_engineer-expand-body');
body.hide();
head.click(function(){
  body.toggle();
  if(head.text() === 'open'){
     head.text('close');
  }else{
     head.text('open');
  }
});
})
$('a.uochan-boldblue').each(function(__CNT__){
$(this).css({color: 'blue', fontWeight: 'bold'})
})
$('a.muddydixon-enlarge').each(function(__CNT__){
$(this).animate({"font-size": "4em"}, 1000)
})
$('div.uochan-radioexpand').each(function(__CNT__){
var speed = parseInt($(this).data("uochan-radioexpand-speed")) || 300,
    checkedInputs = $(this).find("input[type=radio]:checked"),
    targetContent = $(this).children("div.uochan-radioexpand-content");

if(checkedInputs.length === 1){
    targetContent.addClass("uochan-radioexpand-expanded").show();
} else {
    targetContent.hide();
}

$(this).find("input[type=radio]").bind("click", function(e){
    var target = $(e.target);
    var content = target.parents("div.uochan-radioexpand").children("div.uochan-radioexpand-content")[0];

    $("div.uochan-radioexpand-expanded").toggleClass("uochan-radioexpand-expanded", false).slideUp(speed, function(){
        $(content).toggleClass("uochan-radioexpand-expanded", true).slideDown(speed);
    });
});
})
});