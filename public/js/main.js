$(function(){
		var conf = {
// 				baseUrl: 'http://192.168.11.6:8080/jSnippets'
				baseUrl: ''
		};

		
		if($('#snippetList').length > 0){
				$.get(conf.baseUrl+'/snippet', function(snippets){
						$(snippets).each(function(){
								$('#snippetList').append(this);
						});
				});
		}

		// support
		$.getSupportInfo = function(id){
				var snippet = $('div#'+id);
				var supportInfo = snippet.find('div.supports ul');
				var datetime = snippet.find('time');
				$.get(conf.baseUrl+'/support/'+id+'?dt='+datetime.attr('datetime'), function(supports){
						for(var browser in supports){
								if(supports.hasOwnProperty(browser)){
										supportInfo.append('<li class="'+browser+'"><img width=24 height=24 src="/images/browsers/'+browser+'.ico" /><span>'+supports[browser].sum+'</span></div>');
								}
						}
				})
		}
		$.getLike = function(id){
				var snippet = $('div#'+id);
				var likeInfo = snippet.find('div.like span');
				$.get(conf.baseUrl+'/like/'+id, function(res){
						if(res.err && res.err.code === 400){
								alert(res.err.message);
						}else{
								likeInfo.text(res.result);
						}
				})
		}
		$.like = function(){
				var snippet = $(this).parents('div.snippet');
				var likeInfo = snippet.find('div.like span');
				$.post(conf.baseUrl+'/like/'+snippet.attr('id'), function(res){
						if(!res.err){
								var cnt = likeInfo.text();
								cnt++;
 								likeInfo.text(cnt);
						}
				});
				return false;
		}
		$.beautyOfCode.init('clipboard.swf');
		

		// cabinet
		var getSnippet = function(id){
				var snippet = $('#'+id);
				var tagcls = snippet.find('h2.spec').text();
				var supports = snippet.find('div.supports ul li');
				return '<div id="snippet-'+id+'"><h3>'+tagcls+'</h3><input type="hidden" name="sids" value="'+id+'" /></div>'
		};
		var getFileHTML = function(file){
				return $('<li><a href="/file/'+file._id+'">'+file.name+'</a></li>');
		}
		$.get('/cabinet', function(files){
				var myfiles = $('#myfiles ul');
				$(files).each(function(){
						myfiles.append(getFileHTML(this));
				})
		});
		$.addSnippetToFiles = function(snippetId, fileId){
				fileId = fileId || 'new';
				if($('div#snippet-'+snippetId).length === 0){
						$('#selectedSnippets').append(getSnippet(snippetId));
						$('#tray a.tryFile').attr('href', $('#tray a.tryFile').attr('href')+snippetId+'+');
				}
		}
		$.removeSnippetToFiles = function(snippetId, fileId){
				fileId = fileId || 'new';
				$('div#snippet-'+snippetId).remove();
				$('#tray a.tryFile').attr('href', $('#tray a.tryFile').attr('href').replace(snippetId+'+', ''));
		}

    $('#tray a.tryFile').fancybox({width: '75%', height: '75%', autoScale: false, type: 'iframe', transitionIn: 'none', transitionOut: 'none'});
		$('#tray form').submit(function(){
				$.post('/cabinet', $(this).serialize(), function(ev){
						console.log(ev);
				});
				return false;
		})
    $('div.file a.try').fancybox({width: '75%', height: '75%', autoScale: false, type: 'iframe', transitionIn: 'none', transitionOut: 'none'});
});
